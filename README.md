# DUNFORCE Library

Test application for DUNFORCE COMPANY

## Install instructions:

Install backend dependencies: `php bin/console composer install`

Install frontend dependencies: `yarn install` `yarn run build`

Create schema (Execute migrations): `php bin/console doctrine:migrations:migrate`

## Usage

The application is simple. After login with _**admin**_ / _**pass**_ credentials, we have two menu positions:
**Books** and **Authors**. Every position has a submenu that contains links to list and create form. Edit and remove functionalities are presented on resources lists.

## Commands

`php bin/console list:books` - lists books

`php bin/console list:authors` - lists authors