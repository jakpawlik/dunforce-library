<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Author model
 *
 * @ORM\Table(name="app_author", uniqueConstraints={@UniqueConstraint(name="identity", columns={"name", "last_name"})})
 * @ORM\Entity
 *
 * @UniqueEntity(
 *     fields={"name", "lastName"},
 *     errorPath="lastName",
 *     message="This first and last name is already in use."
 * )
 *
 */
class Author {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $lastName;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Book", mappedBy="author",cascade={"persist","remove"}, orphanRemoval=true)
     */
    private $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName().' '.$this->getLastName();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Author
     */
    public function setName(string $name): Author
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Author
     */
    public function setLastName(string $lastName): Author
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getBooks(): ArrayCollection
    {
        return $this->books;
    }

    public function addBook(Book $book): Author
    {
        $this->books->add($book);
        $book->setAuthor($this);

        return $this;
    }

    public function removeBook(Book $book): Author
    {
        $this->books->removeElement($book);

        return $this;
    }
}