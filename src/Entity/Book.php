<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Author model
 *
 * @ORM\Table(name="app_book", uniqueConstraints={@UniqueConstraint(name="uniqeness", columns={"title", "author_id"})})
 * @ORM\Entity
 *
 * @UniqueEntity(
 *     fields={"title", "author"},
 *     errorPath="title",
 *     message="This first and last name is already in use."
 * )
 *
 */
class Book {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $isbn;

    /**
     * @ORM\ManyToOne(targetEntity="Author", inversedBy="books",cascade={"persist"})
     * @ORM\JoinColumn( onDelete="SET NULL")
     *
     */
    protected $author;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\File()
     */
    private $cover;

    /**
     * @var array
     *
     * @ORM\Column(type="simple_array", length=255, nullable=true)
     */
    private $tags;

    public function __construct()
    {
        $this->tags = [];
    }

    public function __toString()
    {
        return $this->getAuthor().' - '.$this->getTitle();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Book
     */
    public function setId(int $id): Book
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Book
     */
    public function setTitle(string $title): Book
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    /**
     * @param string $isbn
     * @return Book
     */
    public function setIsbn(string $isbn): Book
    {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * @return Author
     */
    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     * @return Book
     */
    public function setAuthor($author): Book
    {
        $this->author = $author;

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover($cover): Book
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     * @return Book
     */
    public function setTags(array $tags): Book
    {
        $this->tags = $tags;

        return $this;
    }

}