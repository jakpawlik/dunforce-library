<?php

namespace App\Command;

use App\Entity\Author;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class ListAuthorsCommand extends Command
{
    private $manager;

    /**
     * @param EntityManagerInterface $userManager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;

        parent::__construct();
    }

    /**
     * Configure
     */
    protected function configure()
    {
        $this
           ->setName('list:authors')
           ->setDescription('Lists authors')
        ;
    }

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $authors = $this->manager->getRepository(Author::class)->findAll();
        $table = new Table($output);
        $table
            ->setHeaders(array('Name', 'Last name'));

        $rows = [];

        foreach ($authors as $author) {
            $rows[] = [
                $author->getName(),
                $author->getLastName(),
            ];
        }

        $table->setRows($rows);
        $table->render();
    }
}
