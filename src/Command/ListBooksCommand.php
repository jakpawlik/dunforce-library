<?php

namespace App\Command;

use App\Entity\Book;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class ListBooksCommand extends Command
{
    private $manager;

    /**
     * @param EntityManagerInterface $userManager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;

        parent::__construct();
    }

    /**
     * Configure
     */
    protected function configure()
    {
        $this
           ->setName('list:books')
           ->setDescription('Lists books')
        ;
    }

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $books = $this->manager->getRepository(Book::class)->findAll();
        $table = new Table($output);
        $table
            ->setHeaders(array('ISBN', 'Title', 'Author', 'Tags'));

        $rows = [];

        foreach ($books as $book) {
            $rows[] = [
                $book->getIsbn(),
                $book->getTitle(),
                $book->getAuthor(),
                implode(', ', $book->getTags())
            ];
        }

        $table->setRows($rows);
        $table->render();
    }
}
