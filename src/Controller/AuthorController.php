<?php

namespace App\Controller;

use App\Entity\Author;

class AuthorController extends BaseController
{
    public function __construct()
    {
        $this->setModel(Author::class);
    }
}