<?php

namespace App\Controller;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityRepository;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller {
    protected $model;

    public function indexAction(Request $request): Response
    {

        $queryBuilder = $this->getRepository()->createQueryBuilder('q');
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(10);
        $pagerfanta->setCurrentPage($request->get('page', 1));

        return $this->render($this->getTemplate('index'), [
            'entities' => $pagerfanta
        ]);
    }

    public function showAction(Request $request): Response
    {
        return $this->render($this->getTemplate('show'), [
            'entity' => $this->find($request->get('id'))
        ]);
    }

    public function editAction(Request $request): Response
    {
        $name = $this->getResourceName();
        $entity = $this->find($request->get('id'));

        if (!$entity) {
            $entity = new $this->model;
        }

        $form = $this->createForm('App\\Form\\'.$name.'Type', $entity);

        $form->add('save', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();


            $em->persist($entity);
            $em->flush();
            return $this->redirectToRoute($name.'Edit', ['id' => $entity->getId()]);
        }

        return $this->render($this->getTemplate('edit'), [
            'entity' => $entity,
            'edit' => $entity->getId() !== null,
            'form' => $form->createView()
        ]);
    }

    public function removeAction(Request $request): Response
    {
        $name = $this->getResourceName();

        $entity = $this->find($request->get('id'));
        $em = $this->getDoctrine()->getManager();

        $em->remove($entity);
        $em->flush();

        return $this->redirectToRoute($name.'Index');
    }

    protected function find(int $id = null)
    {
        return $id ? $this->getRepository()->find($id) : null;
    }
    
    protected function setModel(string $modelClass): BaseController
    {
        $this->model = $modelClass;

        return $this;
    }

    protected function getResourceName(): string
    {
        $data = explode('\\',$this->model);

        return $data[sizeof($data) - 1];
    }

    protected function getTemplate($view, $format = 'html'): string
    {
        $name = $this->getResourceName();

        return $name.'\\'.$view.'.'.$format.'.twig';
    }

    protected function getRepository(): EntityRepository
    {
        return $this->getDoctrine()->getRepository($this->model);
    }
}