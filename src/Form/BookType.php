<?php
namespace App\Form;

use App\Entity\Author;
use App\Entity\Book;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'widget_form_group_attr' => array(
                    'class' => 'col-sm-3',
                )
            ])
            ->add('author', EntityType::class, [
                'class' => Author::class,
                'widget_form_group_attr' => array(
                    'class' => 'col-sm-3',
                )
            ])
            ->add('cover', FileType::class, [
                'required' => false,
                'data_class' => null,
                'widget_form_group_attr' => array(
                    'class' => 'col-sm-3',
                )
            ])
            ->add('isbn', TextType::class, [
                'widget_form_group_attr' => array(
                    'class' => 'col-sm-3',
                )
            ])
            ->add('tags', TextType::class, [
                'label' => 'Tags (comma - separated)',
                'widget_form_group_attr' => array(
                    'class' => 'col-sm-3',
                )
            ])
        ;

        $builder->get('tags')->addModelTransformer(new CallbackTransformer(
            function($item) {
                return implode(',', $item);
            }, function($item) {
            return explode(',', $item);
        }));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}