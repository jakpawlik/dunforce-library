<?php
namespace App\Form\Extension\CollectionType;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollectionTypeExtension extends AbstractTypeExtension
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'entry_options' => [
                'label' => false,
                'label_render' => false
            ]
        ));
    }

    public function getExtendedType()
    {
        return CollectionType::class;
    }
}