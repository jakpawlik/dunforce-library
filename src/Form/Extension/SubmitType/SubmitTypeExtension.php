<?php
namespace App\Form\Extension\SubmitType;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class SubmitTypeExtension extends AbstractTypeExtension
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {

        $view->vars['widget_form_group'] = $options['widget_form_group'];
        $view->vars['widget_form_group_attr'] = $options['widget_form_group_attr'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'widget_form_group_attr' => [ 'class' => 'col-sm-2' ],
            'widget_form_group' => true,
        ));
    }

    public function getExtendedType()
    {
        return SubmitType::class;
    }
}