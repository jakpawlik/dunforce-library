<?php
namespace App\Form\Extension\DateType;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class DateTypeExtension extends AbstractTypeExtension
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['format'] = $options['format'];
        $view->vars['ui_format'] = $options['ui_format'];
        $view->vars['datepicker'] = $options['datepicker'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'format' => 'yyyy-MM-dd',
            'ui_format' => 'yy-mm-dd',
            'datepicker' => true
        ));
    }

    public function getExtendedType()
    {
        return DateType::class;
    }
}