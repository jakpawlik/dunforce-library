<?php
namespace App\Form\Extension\FormType;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormTypeExtension extends AbstractTypeExtension
{
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['omit_collection_item'] = $options['omit_collection_item'];
        $view->vars['widget_form_group'] = $options['widget_form_group'];
        $view->vars['label_render'] = $options['label_render'];
        $view->vars['widget_form_group_attr'] = $options['widget_form_group_attr'];
        $view->vars['datepicker'] = $options['datepicker'];

        if (!$options['label']) {
            $view->vars['label'] = ucfirst(str_replace('_', ' ', $form->getName()));
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'widget_form_group' => true,
            'widget_form_group_attr' => [ 'class' => '' ],
            'datepicker' => false,
            'label_render' => true,
            'omit_collection_item' => false,
        ));
    }

    public function getExtendedType()
    {
        return FormType::class;
    }
}